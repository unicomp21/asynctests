﻿// AsyncTests.cpp : Defines the entry point for the application.
//

#include "AsyncTests.h"

using namespace std;

class CaptureMe {
private:
	std::function<bool(int arg1, int arg2)> _method1;
private:
	std::function<bool(int arg1, int arg2)> _method2;
private:
	int _captureVar;
private:
	bool testBind(int arg1, int arg2) {
		cout << __FUNCTION__ << ": arg1 = " << arg1 << ", arg2 = " << arg2 << std::endl;
		return true;
	}
public:
	void Run() {
		auto check1 = _method1(1, 2);
		cout << "check1 = " << check1 << std::endl;
		auto check2 = _method2(3, 4);
		cout << "check2 = " << check2 << std::endl;
	}
public:
	CaptureMe(int val = 3): _captureVar(val) {
		_method1 = [captureVar = _captureVar](int arg1, int arg2) {
			cout << "testLambda: captureVar = " << captureVar << ", arg1 = " << arg1 << ", arg2 = " << arg2 << endl;
			return true;
		};
		_method2 = std::bind(&CaptureMe::testBind, this, std::placeholders::_1, std::placeholders::_2);
	}
public:
	~CaptureMe() {
		cout << "destroy: " << __FUNCTION__ << endl;
	}
};


int main()
{
	cout << "Hello CMake." << endl;
	CaptureMe captureMe(5);
	captureMe.Run();

	return 0;
}
